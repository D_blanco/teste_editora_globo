var default_error_callback = function(data) { console.log('CEP inexistente', data); };
var default_success_callback = function(data) { console.log('CEP encontrado com sucesso.', data); };
var default_complete_callback = function(data) { console.log('AutoCep completado'); };
var default_before = function() { console.log('AutoCep inicializando...'); };

function AutoCep(options) {

    //console.log("options", options)

    var cep_field_selector = options.cep_selector || 'input[name="cep"]';
    var bairro_field_selector = options.bairro_selector || 'input[name="bairro"]';
    var logradouro_field_selector = options.endereco_selector || 'input[name="endereco"]';
    var uf_field_selector = options.estado_selector || 'input[name="estado"]';
    var localidade_field_selector = options.cidade_selector || 'input[name="cidade"]';

    var before_action = options.before || default_before;
    var error_callback = options.onError || default_error_callback;
    var success_callback = options.onSuccess || default_success_callback;
    var complete_callback = options.onComplete || default_complete_callback;

    //console.log("cep_field_selector", cep_field_selector)

    var onfocusout = function () {

        before_action();

        var cep = $(cep_field_selector).val().replace('-', '');

        $.ajax({
            url: 'https://viacep.com.br/ws/' + cep + '/json',
            type: 'GET',
            success: function(data) {
                if (data.erro) {
                    $('.loading').hide();
                    return;
                }

                $(bairro_field_selector).val(data.bairro);
                $(logradouro_field_selector).val(data.logradouro);
                $(uf_field_selector).val(data.uf).trigger('change');
                $('.loading').show();
                setTimeout(function() {
                    $('.loading').hide();
                    var cidade = data.localidade.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
                    $(localidade_field_selector).val(cidade).trigger('change');
                }, 500);


                success_callback(data);
                complete_callback();
            },
            error: function(data) {
                error_callback(data);
                complete_callback();
            }
        });

    };

    $(cep_field_selector).focusout(onfocusout);

};
