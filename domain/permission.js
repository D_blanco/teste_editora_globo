// middleware for doing role-based permissions
module.exports = function permit(...allowed) {
    // console.log(allowed)
const isAllowed = role => allowed[0] == 'ALL' || role.includes('ADMIN') || allowed.some(a => role.includes(a));

    // return a middleware
    return (req, res, next) => {
        if (req.user && isAllowed(req.user.tipo))
            next(); // role is allowed, so continue on the next middleware
        else {
            req.flash('danger', 'Você não tem permissão para executar esta ação.')
            return res.redirect('/login');
        }
    }
}