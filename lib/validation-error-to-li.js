module.exports = function(errors) {
    var errors_string = "<ul>";
    for (var x in errors) {
        errors_string += "<li>" + errors[x].msg + "</li>";
    }
    errors_string += "<ul>";
    return errors_string;
};