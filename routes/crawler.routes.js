const PromiseCrawler = require('promise-crawler');

var X2JS = require('x2js');
var x2js = new X2JS();

exports.start_crawler = async (req, res, next) => {

    try{

        const crawler = new PromiseCrawler({
            maxConnections: 10,
            retries: 0
        });

        await crawler.setup();
        let response = await crawler.request({url: 'http://revistaautoesporte.globo.com/rss/ultimas/feed.xml'});

        let data = response.body;

        var json = x2js.xml2js(data);

        let rootFeed = json.rss.channel.item;

        let feed = [];

        for (let item of rootFeed){

            let obj = {
                title: item.title,
                link: item.link,
                description: []
            };

            let descriptionData = await crawler.request({html: item.description});

            let $ = descriptionData.$;

            let allTextTag = $('p').text();
            let allLinkTag = $('a');
            let allImgTag = $('img');

            allLinkTag = await Object.values(allLinkTag);
            allImgTag = await Object.values(allImgTag);

            let links = [];
            let images = [];

            allLinkTag.forEach((val) => {if(val.attribs) links.push(val.attribs.href); });
            allImgTag.forEach((val) => {if(val.attribs) images.push(val.attribs.src); });

            obj.description.push({type: 'text', content: allTextTag});
            obj.description.push({type: 'images', content: images});
            obj.description.push({type: 'links', content: links});

            feed.push(obj);
        }


        return res.send({feed: feed});

    } catch (e) {
        console.log('Error', e);
        next()
    }


};