var requireGuest = require('../lib/require-guest');
var requireLogin = require('../lib/require-login');
var fileupload = require("express-fileupload");

// Controllers

var user = require('./users.routes');
var sessions = require('./sessions.routes');
var crawler = require('./crawler.routes');


var permit = require('../domain/permission');


const maintenance_mode = process.env.MAINTENANCE_MODE || "NO";

module.exports = function(app) {

    app.use(function(req, res, next) {

        if (maintenance_mode == "YES") {
            return res.render('manutencao');
        }

        next();

    });

    app.use(fileupload());

    // app.get('/usuarios', permit("ADMIN"), requireLogin, user.index);
    // app.get('/usuarios/create', permit("ADMIN", "COMUM"), requireLogin, user.create);
    // app.post('/usuarios', permit("ADMIN"), requireLogin, user.store);
    // app.get('/usuarios/:id/edit', permit("ADMIN"), requireLogin, user.edit);
    // app.get('/usuarios/:id/desativar', permit("ADMIN"), requireLogin, user.desativar);
    // app.get('/usuarios/:id/ativar', permit("ADMIN"), requireLogin, user.ativar);
    // app.post('/usuarios/:id', permit("ADMIN"), requireLogin, user.update);
    // app.get('/usuarios/:id/delete', permit("ADMIN"), requireLogin, user.delete);


    // Admin
    app.get('/', permit("ADMIN"), crawler.start_crawler);


    app.get('/get-feed/require-auth', permit("ADMIN"), crawler.start_crawler);
    app.get('/get-feed', crawler.start_crawler);

    // Users
    // app.get('/register', requireGuest, user.registerForm);
    // app.post('/register', user.register);

    // Sessions
    app.get('/login', requireGuest, sessions.loginForm);
    app.post('/login', sessions.login);
    app.post('/change-password', sessions.new_pass);
    app.get('/logout', sessions.logout);


};
